# Multilatex Presentation

## Build it

Get the dependencies:

    yarn

Build it:

    yarn build

## Images used:

- http://www.epstudiossoftware.com/wp-content/uploads/2010/08/teletype_asr33.jpg

## License

MIT

# Multilatex (lucrarea de dizertație)

![screenshot](screenshot.png)

## Pentru lucru ușor

Trebuie instalat (în Ubuntu):

    sudo apt-get install inotify-tools

Pentru compilare:

    make

Pentru ascultare și compilare:

    make listen

Pentru curățare

    make clean
